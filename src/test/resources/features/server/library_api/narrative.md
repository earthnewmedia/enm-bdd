@release:v0.1a
Library API

The Library API is responsible for general Library operations outside the context of a specific user, such as searching, name lookup, and managing Library Content.

**Endpoints**

- `GET /user-library/count` Count User Libraries
- `GET /user-library/{librarySlug}` Get User Library
- `GET /user-library` Find User Libraries
- `POST /user-library` Create User Library
- `PATCH /user-library/{libraryId}` Update User Library
- `DELETE /user-library/{libraryId}` Delete User Library