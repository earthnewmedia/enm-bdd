@server
@library_api
@release:v0.1a
Feature: Update User Library API Endpoint

        The `PATCH /user-library/{libraryId}` enpoint is used to update a
        `UserLibrary` entity which belong to the principal `User`

    @version:0.1.0
    Scenario: Use api to update library
        Given that an admin was able to initialize the system under test
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        And he was able to login to the api
        And he was able to use the api to create a library with
            | access      | Public         |
            | libraryType | Discussion     |
            | name        | My Discussions |
            | description | My Description |
        When he attempts to use the api to update the library with
            | access | Private   |
            | name   | The Album |
        Then he should see that they recieved a NoContent status code
