@release:v0.1a
User API

The User Api provides standard CRUD endpoints for the User entity, and its relations
(EG `attributes` and `libraries`).

**Endpoints**

- `GET ​/user​/me` (get principal)
- `GET ​/user​/{userSlug}` (get user)
- `GET /user/count` (count users)
- `POST /user` (create new user)
- `GET /user` (find users)
