@server
@auth_api
@release:v0.1a
Feature: API Login

   Background:
      Given that an admin was able to initialize the system under test
      And that "Jane" is a Member with the account data
         | slug     | jane-doe         |
         | email    | jane@example.com |
         | password | password3        |

   @version:0.1.0
   Scenario: Login to api with valid credentials
      When "Jane" attempts to login to the api
      Then she should see that they recieved a valid LoginSession response

   @version:0.1.0
   Scenario: Login to api with invalid credentials
      When "Jane" attempts to login to the api with the credentials
         | email    | jane@example.com |
         | password | wrongpassword    |
      Then she should see that they recieved an Unauthorized status code
