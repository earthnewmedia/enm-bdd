@server
@auth_api
@release:v0.1a
Feature: API Login Refresh

  The `/login/refresh` endpoint should renew the login session
  with an updated expire date

  Background:
    Given that an admin was able to initialize the system under test
    And that "Jane" is a Member with the account data
        | slug     | jane-doe         |
        | email    | jane@example.com |
        | password | password3        |

  @version:0.1.1
  Scenario: Login to api with valid credentials
    Given that "Jane" was able to login to the api
    When she attempts to use the api to refresh the login session
    Then she should see that they recieved a valid LoginSession response

