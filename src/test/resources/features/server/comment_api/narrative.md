@release:v0.1a
Comment Api

The Comment API is responsible for mannaging Comment entities withing the bounds of a specific Content entity.

**Endpoints**

- GET `/comment/count` Count Comments
- GET `/comment/{commentId}` Get Comment Details
- GET `/comment` Find Comments