@release:v0.1a
Search API

The Search API is responsible for taking in generic text strings and attempts
to match it to various entity fields in a preferred sort order.