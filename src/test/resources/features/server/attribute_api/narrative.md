@release:v0.1a
User Attribute API

The User Attribute API provides standard CRUD functionality for the `UserAttribute` entity.

- `POST /user-attribute` (create user attribute)
- `GET /user-attribute/count` (count user attributes)
- `GET /user-attribute` (get user attributes)
- `PATCH /user-attribute/{attributeId}` (update user attribute)
- `DELETE /user-attribute/{attributeId}` (delete user attribute)