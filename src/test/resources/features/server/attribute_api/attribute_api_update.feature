@server
@attribute_api
@release:v0.1a
Feature: Update User Attribute API Endpoint

    The `PATCH /user-attribute/{attributeId}` enpoint may be used to
    update an existing `UserAttribute` which belongs to the principal `User`

    @version:0.1.0
    Scenario: Use api to update attribute
        Given that an admin was able to initialize the system under test
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        And he was able to login to the api
        And he was able to use the api to create a library with
            | access      | Public         |
            | libraryType | Discussion     |
            | name        | My Discussions |
            | description | My Description |
        And he was able to use the api to create an attribute called "Foo" with the content
            """
            [waweb.io](https://www.waweb.io)
            """
        When he attempts to use the api to update the attribute with
            | label | Bar |
        Then he should see that they recieved a NoContent status code
