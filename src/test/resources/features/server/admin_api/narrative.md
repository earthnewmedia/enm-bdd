@release:v0.1a
Admin API

  The Admin API is responsible for preforming restricted actions that are not normally allowed by standard users.

  **Endpoints**

- GET `/ping` (hello world)
- GET `/admin/reset` (reset test fixtures)

- PATCH `/admin/user` (update all users)
- PUT `/admin/user/{userId}` (replace user)
- DELETE `/admin/user/{userId}` (delete user)

- PATCH `/admin/library` (update all libraries)
- PUT `/admin/library/{libraryId}` (replace library)

- PATCH `/admin/content` (update all content)
- PUT `/admin/content/{contentId}` (replace content)

- PATCH `/admin/comment` (update all comments)
- DELETE `/admin/comment/{commentId}`
- PUT `/admin/comment/{commentId}` (replace comment)