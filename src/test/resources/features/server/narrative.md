enm-server

The initial earthnew.media server is a simple single-host rest api for domain model CRUD actions. Later versions of the server will included additional microservices for email, messaging, and logger support.
