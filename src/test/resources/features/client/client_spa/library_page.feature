@client_spa
@library_page
@release:v0.1a
Feature: Library Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "Jane" is a Member with the account data
			| slug     | janedoe          |
			| email    | jane@example.com |
			| userName | JaneDoe          |
			| password | password3        |
		And she was able to start on thier own member home page

	@version:0.1.0
	Scenario: Create new library
		When "Jane" attempts to create a new library with
			| access      | Public         |
			| libraryType | Discussion     |
			| name        | My Discussions |
			| description | Discussions    |
		Then she should see that they are on the "janedoe/my-discussions" library page
		And the library access is "Public"
		And the library type is "Discussion"
		And the library name is "My Discussions"
		And the library description is "Discussions"
		And the library was created "Today"
		And the delete library button is available

	@version:0.1.0
	Scenario: Navigate to library page
		Given that "Jane" was able to create a new library with
			| access      | Private     |
			| libraryType | Album       |
			| name        | My Album    |
			| description | Description |
		And she was able to navigate to thier own member home page
		When she attempts to navigate to the "janedoe/my-album" library page
		Then she should see that they are on the "janedoe/my-album" library page
		And the library access is "Private"
		And the library type is "Album"
		And the library name is "My Album"
		And the library was created "Today"
		And the library was last modified "Today"

	@version:0.1.0
	Scenario: Delete library
		Given that "Jane" was able to create a new library with
			| access      | Private     |
			| libraryType | Album       |
			| name        | My Album    |
			| description | Description |
		When she attempts to delete the library
		Then she should see that they are on thier own member home page
		And the member library list is empty

	@version:0.1.0
	Scenario: Update existing library
		Given that "Jane" was able to create a new library with
			| access      | Private     |
			| libraryType | Album       |
			| name        | My Album    |
			| description | Description |
		When she attempts to change the library access to "Public"
		And she refreshes the page
		Then she should see that the library access is "Public"
