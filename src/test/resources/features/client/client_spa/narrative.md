@release:v0.1a
Client SPA

**Page Routes**

- `/#` Landing Page
- `/#/login` Login Page
- `/#/logout` Logout Page (redirect -> `/#`)
- `/#/register` Registration Page
- `/#/user/{userSlug}` Member Home Page
- `/#/user/{userSlug}/account` Member Account Page (private)
- `/#/user/{userSlug}/profile` Member Profile Page
- `/#/library/{librarySlug}` Library Page
- `/#/content/{contentSlug}` Content Page
- `/#/comment/{threadId}` Comment Thread Page
