@client_spa
@profile_page
@release:v0.1a
Feature: Profile Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "John" is a Member with the account data
			| slug     | johndoe          |
			| email    | john@example.com |
			| userName | JohnDoe          |
			| password | password2        |
		And he was able to start on thier own member home page

	@version:0.1.0
	Scenario: Navigate to own profile page
		When he attempts to navigate to thier own profile page
		Then he should see that they are on thier own profile page
		And the member attribute list widget is available
		And the member attribute list is empty

	@version:0.1.0
	Scenario: Adding member profile attributes
		Given "John" was able to navigate to thier own profile page
		When he attempts to add the "Home Page" attribute with
			"""
			https://www.waweb.io
			"""
		And he attempts to add the "Languages" attribute with
			"""
			- Groovy
			- Typescript
			- Rust
			"""
		Then he should see that the attribute list contains a "Home Page" item with a link to "https://www.waweb.io"
		Then the attribute list contains a "Languages" item with the list items
			| Groovy     |
			| Typescript |
			| Rust       |

