@client_spa
@landing_page
@release:v0.1a
Feature: Landing Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "Bob" is a is a Guest without an account

	@version:0.1.0
	Scenario: View landing page
		When "Bob" attempts to view the landing page
		Then he should see that they are on the landing page
		Then the home nav is available
		And the login nav is available
		And the register nav is available
		And the search widget is available
		And the hero image is available
