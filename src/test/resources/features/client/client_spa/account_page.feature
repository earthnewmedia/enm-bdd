@client_spa
@account_page
@release:v0.1a
Feature: Account Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "John" is a Member with the account data
			| slug     | johndoe          |
			| email    | john@example.com |
			| userName | JohnDoe          |
			| password | password2        |
		And he was able to start on thier own member home page

	@version:0.1.0
	Scenario: Navigate to own account page
		When "John" attempts to navigate to the member account page
		Then he should see that they are on the member account page

	@version:0.1.1
	Scenario: Update user credentials
		Given that "John" was able to start on thier own member account page

	@version:0.1.2
	Scenario: Rename member account
		Given that "John" was able to start on thier own member account page
