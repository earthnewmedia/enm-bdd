/**
 * 
 */
package earthnewmedia.bdd.api

import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.api.tasks.ApiPost
import earthnewmedia.bdd.api.tasks.ApiTask
import io.cucumber.java.PendingException

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseTheAuthApi {
    
    static ApiTask loginToApi() {
        return instrumented(ApiPost, Endpoint.Auth.login())
    }
    
    static ApiTask refreshTheLoginSession() {
        throw new PendingException("TODO: not implemented")
    }
}
