/**
 * 
 */
package earthnewmedia.bdd.api.questions

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.rest.questions.LastResponse

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ResponseBody implements Question<Object> {
    
    public static ResponseBody theResponseBody(Closure wrapper = null) {
        return new ResponseBody(wrapper)
    }

    final Closure wrapper

    @Override
    public Object answeredBy(Actor actor) {
        def lastResponse =  LastResponse.received().answeredBy(actor)
        return wrapper ? wrapper(lastResponse.jsonPath()) : lastResponse.jsonPath().get()
    }
    
    private ResponseBody(Closure wrapper) {
        this.wrapper = wrapper
    }
}
