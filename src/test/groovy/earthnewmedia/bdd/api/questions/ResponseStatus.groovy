/**
 * 
 */
package earthnewmedia.bdd.api.questions

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.rest.questions.LastResponse

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ResponseStatus implements Question<Integer> {

    public static ResponseStatus theResponseStatus() {
        return new ResponseStatus()
    }
    
    @Override
    public Integer answeredBy(Actor actor) {
        def lastResponse = LastResponse.received().answeredBy(actor)
        return lastResponse.statusCode()
    }
}
