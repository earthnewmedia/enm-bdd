/**
 * 
 */
package earthnewmedia.bdd.api.tasks

import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.interactions.Delete
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiDelete extends ApiTask {

    @Step("{0} attempts to DELETE from #resource")
    <T extends Actor> void performAs(T actor) {
        assert resource
        actor.attemptsTo Delete.from(getEndpointUri()).withRequest({ RequestSpecification req ->
            prepareRequest(actor, req)
        })
    }
    
    ApiDelete(String resource) {
        this.resource = resource
    }
}
