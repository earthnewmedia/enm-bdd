/**
 * 
 */
package earthnewmedia.bdd.api.tasks

import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.interactions.Post
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiPost extends ApiTask {

    @Step("{0} attempts to POST to #resource")
    <T extends Actor> void performAs(T actor) {
        assert resource && payload
        actor.attemptsTo Post.to(getEndpointUri()).withRequest({ RequestSpecification req ->
            prepareRequest(actor, req).body(payload)
        })
    }

    ApiPost(String resource, Object payload = null) {
        this.resource = resource
        this.payload = payload
    }
}
