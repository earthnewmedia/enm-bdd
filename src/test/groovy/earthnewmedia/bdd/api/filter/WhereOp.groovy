package earthnewmedia.bdd.api.filter

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 *  See: https://loopback.io/doc/en/lb3/Where-filter.html
 */
enum WhereOp {
    eq,
    or,
    gt,
    gte,
    lt,
    lte,
    between,
    inq,
    nin,
    near,
    neq,
    like,
    nlike,
    ilike,
    nilike
}
