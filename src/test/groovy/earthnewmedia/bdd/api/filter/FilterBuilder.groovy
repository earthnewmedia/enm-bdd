/**
 * 
 */
package earthnewmedia.bdd.api.filter

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * Creates a filter Map compatible with the rest api. Filters may define
 * relation inclusion and/or where conditions
 */
class FilterBuilder {

    // static Map mapWhereClause(WhereClause clause) {
    //   final Map pair = [ [clause.key] : clause.value ]
    //   return (clause.op == WhereOp.eq) ?
    //        pair : [ [clause.op] : pair ]
    // }

    static FilterBuilder query() {
      return new FilterBuilder()
    }

    final List<String> relations = []
      
    final List<WhereClause> filters = []

     FilterBuilder where(List<WhereClause> filters) {
        this.filters.addAll(filters)
        return this
    }

    FilterBuilder withRelations(List<String> relations) {
        this.relations.addAll(relations)
        return this
    }

    String build() {
      if(relations.isEmpty() && filters.isEmpty()) {
        return ""
      }

      if(filters.size() == 1) {
        def clause = filters.first()
        if(clause.op == WhereOp.eq) {
          return "where[${clause.key}]=${clause.value}"
        }
      }

      throw new RuntimeException("TODO: not implemented")
    }
}
