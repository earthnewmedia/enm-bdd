/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.server.rest_api

import static earthnewmedia.bdd.api.UseTheAdminApi.*

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class AdminApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }
    
    @When("{string} attepmts to use the api to reset the test fixtures")
    void attepmts_to_use_the_api_to_reset_the_test_fixtures(String actor) {
        theActor.whoIsNamed(actor).attemptsTo resetTheTestFixtures()
    }

}
