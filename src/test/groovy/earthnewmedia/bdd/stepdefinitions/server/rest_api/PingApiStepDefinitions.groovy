/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.server.rest_api

import static earthnewmedia.bdd.api.UseThePingApi.pingTheApi

import earthnewmedia.bdd.api.tasks.*
import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class PingApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When("{string} attempts to ping the api")
    void attempts_to_ping_the_api(String actor) {
        theActor.whoIsNamed(actor).attemptsTo pingTheApi()
    }

    @When(/s?he attempts to ping the api$/)
    void attempts_to_ping_the_api() {
        theActor.attemptsTo pingTheApi()
    }
}
