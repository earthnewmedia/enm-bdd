/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.client.client_spa

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LoginPageStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given("that {string} was able to navigate to the login page")
    void was_able_to_navigate_to_the_login_page(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @When("{string} attempts to navigate to the login page")
    void attempts_to_navigate_to_the_login_page(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @When("{string} attempts to login to the spa")
    void attempts_to_login_to_the_us(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @When("she attempts to login with the credentials")
    void attempts_to_login_with_the_credentials(io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }

    @Then("she should see that they are on the login page")
    void should_see_that_they_are_on_the_login_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the login form is available")
    void the_login_form_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
}
