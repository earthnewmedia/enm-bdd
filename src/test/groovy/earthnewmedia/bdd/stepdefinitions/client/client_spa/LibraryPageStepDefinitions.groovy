/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.client.client_spa

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LibraryPageStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When("she attempts to navigate to the {string} library page")
    void attempts_to_navigate_to_the_library_page(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Given("that {string} was able to create a new library with")
    void was_able_to_create_a_new_library_with(String string, io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }

    @When("{string} attempts to create a new library with")
    void attempts_to_create_a_new_library_with(String string, io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }

    @When("she attempts to change the library access to {string}")
    void attempts_to_change_the_library_access_to(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @When("she attempts to delete the library")
    void attempts_to_delete_the_library() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("she should see that they are on the {string} library page")
    void should_see_that_they_are_on_the_library_page(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library name is {string}")
    void the_library_name_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library description is {string}")
    void the_library_description_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library type is {string}")
    void the_library_type_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library access is {string}")
    void the_library_access_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the member library list is empty")
    void the_member_library_list_is_empty() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("she should see that the library access is {string}")
    void should_see_that_the_library_access_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library was last modified {string}")
    void the_library_was_last_modified(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the library was created {string}")
    void the_library_was_created(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the delete library button is available")
    void the_delete_library_button_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the create new library widget is available")
    void the_create_new_library_widget_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
}
