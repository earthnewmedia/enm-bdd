/**
 * 
 */
package earthnewmedia.bdd.common

import earthnewmedia.bdd.common.model.User
import net.serenitybdd.screenplay.Ability
import net.serenitybdd.screenplay.Actor

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class Authenticate implements Ability {
    
    String getId() {
        return _user.id
    }
    
    void setId(String id) {
        _user.id = id
    }
    
    String getToken() {
        return _user.token
    }
    
    void setToken(String token) {
        _user.token = token
    }
    
    String getSlug() {
        return _user.slug
    }
    
    String getEmail() {
        return _user.email
    }
    
    String getPassword() {
        return _user.password
    }
    
    private User _user

    @Deprecated
    static Authenticate withCredentials(String email, String password) {
        return new Authenticate(new User(email: email, password: password))
    }
    
    static Authenticate asPrincipal(User user) {
        return new Authenticate(user)
    }

    static Authenticate asPrincipal(Actor actor) throws CannotAuthenticateException {
        // complain if someone's asking the impossible
        if(!actor.abilityTo(Authenticate.class)){
            throw new CannotAuthenticateException(actor.getName())
        }

        return actor.abilityTo(Authenticate.class)
    }

    private Authenticate(User user) {
        _user = user
    }
}
